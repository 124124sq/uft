﻿'Дана любая строка на вход. 
'Заменить все ссылки и email на ***** (количество звездочек равно длине заменяемого фрагмента). 
'Примеры ссылок: www.site.com, http://site.com и т.п. 
'Решить двумя способами: с использованием регулярных выражений и без. Сравнить скорости работы.

'Поиск без выражений
Public function Star(Str)
		All = Split(str) 'делим на части
		'Цикл от начала до конца введённого текста
		For i  = 0 To UBound(All)
			If not InStr(1, All(i), ".com") = 0 Then 'Если в составной части был фрагмент .com тогда меняем его 
				 All(i) = Replace (All(i), All(i), string(len(All(i)), "*"))
			End If
			s = s & All(i) & " " 'Собираем строку обратно
		Next
		'Конец цикла
Msgbox s
End function

'Поиск с Регулярным выражением
Public function StarR(Str)
	Set regEx = New RegExp ' Создать регулярное выражение
	regEx.Pattern = "\S+\.\S+"	' Установить шаблон (\S+)-любые символы, кроме пробела.
	regEx.IgnoreCase = True ' Установить нечувствительность к регистру
	regEx.Global = True ' Установить глобальную применимость
	'Цикл, для каждого совпадения с шаблоном в введённом тексте
	For Each Match in regEx.Execute(Str) 
		Str = Replace (Str, Match.Value, string(len(Match.Value), "*"))  ' Выполнить поиск и замену. 
	next
	'Конец цикла
	msgbox Str
End Function
Star(InputBox ("Введите строку", "Введите строку", "Привет BTV@inbox.com я http://site.com рад тебя видеть. Ты это, заходи на www.site.com хоть иногда"))
StarR(InputBox ("Введите строку", "Введите строку", "Привет BTV@inbox.com я http://site.com рад тебя видеть. Ты это, заходи на www.site.com хоть иногда"))




