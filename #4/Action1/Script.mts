﻿'Описать класс, представляющий треугольник. 
'Предусмотреть методы для создания объектов, вычисления площади, периметра и точки пересечения медиан. 
'Описать свойства для получения состояния объекта.
Dim TRI
'Создаём треугольник и задаём ему стороны
call CreateT(TRI)
'Проверяем введённые данные
If (TRI.a+TRI.b<TRI.c) or (TRI.a+TRI.c<TRI.b) or (TRI.b+TRI.c<TRI.a) Then
	Msgbox "Такого треугольника не существует"
else
	'Если треугольник с такими сторонами может существовать, то считаем его данные
	msgbox "Стороны: " & TRI.a  & " " & TRI.b & " " & TRI.c
	msgbox "Периметр: " & TRI.Sum()
	TRI.S()
	msgbox "Медианы:" & vbCRLF & "К стороне A " & TRI.MedA() & vbCRLF & "К стороне B " & TRI.MedB() & vbCRLF & "К стороне C " & TRI.MedC()
	TRI.Koord' считаем координаты точки пересечения медиан треугольника
End If
if Window("VBS").WinObject("Панель проводника").WinButton("acc_name:=Упорядочить:").Exist then
	Reporter.ReportEvent micDone, "1", "1"
else
	Reporter.ReportEvent micFail, "1", "1"
End if
