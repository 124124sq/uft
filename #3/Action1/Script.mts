﻿'Дан массив. Перемешать его элементы случайным образом так, чтобы каждый элемент оказался на новом месте.
const NUM = 9'Количество элементов массива 
Dim i,j,k,z
'Создаём объект файла для записи массива данных
Set fso = CreateObject("Scripting.FileSystemObject")
Set ts = fso.CreateTextFile("C:\Users\ASYugov\Documents\Unified Functional Testing\Array.txt", 1)
'Создаём массив и записываем его в файл
Dim array()
redim array(NUM)
'Цикл от первого до последнего элемента массива 
for i = 0 to NUM
	array(i) = i
	ts.Write array(i) & " "
Next
'Конец цикла
ts.Writeline
i = 0
Randomize
'Проходим по массиву и меняем положенеи значений массива
for i = 0 to NUM-1
	j =int((NUM-i)*rnd+(i+1))'получаем рандомное Целое число от i до Num
	'будем менять значения массива только с правыми числами,
	'чтобы получить для каждого элемента массива новое рандомное место
	k = array(i)
	array(i) = array(j)
	array(j) = k
	'Цикл от первого до последнего элемента массива 
	for z = 0 to NUM
		ts.Write array(z) & " "
	Next
	'Конец цикла
	ts.Writeline
Next
ts.Close
